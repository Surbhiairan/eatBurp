import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    ScrollView,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { fetchDishSearchResults } from '../../actions/dish.action';
import { fetchCitySpecial } from '../../actions/dish.action';
import SearchBar from '../../components/SearchBar/SearchBar';
import ListCard from '../../components/ListCard/ListCard';
import { recommendDishDispatch } from '../../actions/dish.action';

class CitySpecial extends Component {

    static navigatorStyle = {
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        this.state = {
            selected: "restaurant",
            restaurant: true,
            dishes: false,
            props: null
        }
    }

    componentDidMount() {
        this.props.fetchCitySpecial();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.citySpecial && nextProps.citySpecial.length>0)
        this.setState({props: nextProps.citySpecial[0].city_special_restaurant_dish});
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress") {
            if (event.id === "sideDrawerToggle") {
                this.props.navigator.toggleDrawer({
                    side: "left"
                });
            }
        }
    };

    listCardPressedHandler = (item) => {
        if (this.state.restaurant) {
            this.props.navigator.push({
                screen: "DishDetailScreen",
                passProps: {
                    selectedDish: item.item
                }
            })
        } else {
            this.props.fetchDishSearchResults(item.search_tag);
            console.log("item in itemPressHandler", item)
            this.props.navigator.push({
              screen: "SearchResultScreen",
              title: item.search_tag
            });
        }
    }

    renderListComponent = (item) => {
        if(this.state.restaurant === true)
        return(
            <ListCard 
                dish = {item.item}
                type= "dishRestaurantMapping"
                restaurant_name = {item.item.restaurant_name}
                locality = {item.item.locality}
                dish_rating={item.item.average_rating}
                dish_name={item.item.dish_name}
                price={item.item.price}
                image = {item.item.images}
                onPress={() => this.listCardPressedHandler(item)}
                onPressReview={this.reviewButtonPressHandler}
            />
            )
        else
        return(
             
            <ListCard 
                dish = {item.item}
                type = "dish"
                dish_name = {item.item.dish_name}
                image = {item.item.images}
                onPress = {() => this.listCardPressedHandler(item)}
            />
        )
    }

    // reviewButtonPressHandler = (dish) => { 
    //     this.props.navigator.push({
    //         screen: "ReviewDishScreen",
    //         title: 'Add Review',
    //         passProps: {
    //             selectedDish: dish
    //         }
    //     });
    //     alert("reviewed");
    // }

    reviewButtonPressHandler = dish => {
      console.log("Dish--------", dish);
      let alreadyReviewed = false;
      //dispatch action to increase recommendation count, pass dish_restaurant_mapping id
      console.log(" user reviews", this.props.reviews)
      for (let i = 0; i < this.props.reviews.length; i++) {
        if(this.props.reviews[i].dish_detail){                            
          if (this.props.reviews[i].dish_detail._id === dish._id) {
              alreadyReviewed=true
              if(this.props.reviews[i].review.review === "") {
                  console.log("you have already rated this dish");
                  Alert.alert(
                      '',
                      'you have already rated this dish. Do you want to write a Review?',
                      [
                          { text: 'OK', onPress: () => this.moveToReviewPage(dish, this.props.reviews[i].review)},
                          { text: 'Cancel'}
                      ],
                     
                  )
                  break;
              } else if(this.props.reviews[i].review.rating === 0 ){
                  console.log("you have already reviewed this dish. Do you want to rate it?");
                  Alert.alert(
                      '',
                      'you have already reviewed this dish. Do you want to rate it?',
                      [
                          { text: 'OK', onPress: () => this.moveToReviewPage(dish, this.props.reviews[i].review)},
                          { text: 'Cancel'}
                      ],
                     
                  )
                  break;
              }
              
              else {
                  Alert.alert(
                      '',
                      'you have already reviewed it. Please check your profile for more detail.',
                      [
                          { text: 'OK', onPress: () => console.log('OK Pressed') },
                          { text: 'Cancel', }
                      ],
                      
                  )
                  break;
              }
          }
      }
    }
      if(alreadyReviewed===false)
      this.moveToReviewPage(dish, null);
    }
    
    moveToReviewPage = (dish, alreadyReviewed) => {
        console.log("moving to review page.", dish);
        console.log("already review----------", alreadyReviewed);
        this.props.navigator.push({
            screen: "ReviewDishScreen",
            title: 'Add Review',
            passProps: {
                selectedDish: dish,
                alreadyReviewed: alreadyReviewed
            }
        });
    }

    onButtonPress = (type) => {
        this.setState({ selected: type })
    }

    backIconPress = () => {
        this.props.navigator.pop();
    }
    searchBarPressHandler = () => {
        //navigate to search suggestion screen
        this.props.navigator.push({
            screen: "SearchSuggestionScreen",
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <View style={style.header}>
                <View style={style.backIconContainer}>
                    <TouchableOpacity onPress={this.backIconPress}>
                        <Icon name="ios-arrow-round-back-outline" size={45} color="#fff" />
                    </TouchableOpacity></View>
                    <View style={style.searchbar}>
                        <SearchBar
                            onSearchBarPressed={this.searchBarPressHandler}
                        />
                    </View >
                </View>
                <Text style={style.citySpecial}>City Special</Text>
                <View style={style.tabBar}>
                    <TouchableOpacity onPress={() => this.setState({ restaurant: true, dishes: false })}>
                        <View elevation={5} style={[this.state.restaurant ? style.selectedTab : style.tab]}>
                            <Text style={[this.state.restaurant ? style.selectedTabText : style.tabText]}>Restaurant</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ restaurant: false, dishes: true })}>
                        <View elevation={5} style={[this.state.dishes ? style.selectedTab : style.tab]}>
                            <Text style={[this.state.dishes ? style.selectedTabText : style.tabText]}>Dish</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                {this.props.citySpecialError ? 
                    (Alert.alert(
                      'Oops!',
                      'Please refresh!',
                      [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                    )
                    ):
                (<View style={{ flex: 1}}>
                    {
                        (this.state.restaurant) &&
                        (
                            this.props.citySpecialLoading ? <ActivityIndicator /> : (
                            <FlatList
                                data={this.state.props}
                                renderItem={this.renderListComponent}
                                keyExtractor={(item, index) => index.toString()}
                            />)
                        )
                    }
                    {
                        (this.state.dishes) &&
                        
                        <FlatList 
                            numColumns={2}
                            data={this.props.citySpecial[0].city_special_dishes}
                            renderItem={this.renderListComponent}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    }
                </View>)}

            </View>
        );
    }
}

const style = StyleSheet.create({
    header: {
        backgroundColor:'#009e96',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop:'2%',
        paddingBottom:'1%'
    },
    backIconContainer:{
        //marginTop: '5%',
        marginLeft: '5%',
    },
    searchbar: {
        flex:1,
        //borderColor: '#fff',
        backgroundColor:"#fff",
        //borderWidth: 1,
        alignItems: 'center',
        borderRadius:15,
        //marginTop: '5%',
        marginLeft: '5%',
        marginRight:'5%'
    },
    citySpecial: {
        paddingLeft: '5%',
        paddingTop: '1%',
        paddingBottom: '1%',        
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 22,
        color: '#212121'
    },
    tabBar: {
        flexDirection: 'row',
        paddingLeft: 15,
    },
    selectedTab: {
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#009e96',
        borderRadius: 18,
        margin: 5,
    },
    tab: {
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#fff',
        borderRadius: 18,
        margin: 5,
    },
    selectedTabText: {
        padding: 6,
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 15,
        color: '#fff'
    },
    tabText: {
        padding: 6,
        fontFamily: 'OpenSans-Bold',
        fontSize: 13,
        color: '#212121'
    },
})

const mapStateToProps = state => {
    console.log("city special-------", state.dish)
    return {
        citySpecial: state.dish.citySpecial,
        citySpecialLoading: state.dish.citySpecialLoading,
        citySpecialError: state.dish.citySpecialError,
        reviews: state.reviews.reviews,
        reviewsError: state.reviews.reviewsError,
        reviewsLoading: state.reviews.reviewsLoading,
        recommendations: state.reviews.recommendations,
        recommendationsError: state.reviews.recommendationsError,
        recommendationsLoading: state.reviews.recommendationsLoading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchCitySpecial: () => dispatch(fetchCitySpecial()),        
        fetchDishSearchResults: (searchTag) => dispatch(fetchDishSearchResults(searchTag)),        
        recommendDishDispatch: (dish_id) => dispatch(recommendDishDispatch(dish_id)),
        //reviewDishDispatch: (dish_id) => dispatch(reviewDishDispatch(dish_id)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(CitySpecial);