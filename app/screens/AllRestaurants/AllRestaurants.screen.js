import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    FlatList
} from 'react-native';

import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { fetchSelectedRestaurant } from '../../actions/restaurant.action';

class AllRestaurants extends Component {

    static navigatorStyle = {
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress") {
            if (event.id === "sideDrawerToggle") {
                this.props.navigator.toggleDrawer({
                    side: "left"
                });
            }
        }
    };

    backIconPress = () => {
        this.props.navigator.pop();
    }

    restaurantPressedHandler = (restaurant) => {
        //console.log("here------------", dish)
        this.props.fetchSelectedRestaurant(restaurant._id);
        this.props.navigator.push({
            screen: "RestaurantDetailScreen",
            title: restaurant.restaurant_name,
            passProps: {
                id: restaurant._id
            }
        });
    }

    renderRestaurant = (restaurant) => {
        return(
            <TouchableOpacity onPress={() => this.restaurantPressedHandler(restaurant.item)}>
                <View style={style.restaurantContainer}>
                <Text style={{fontSize:20, fontFamily:'OpenSans-Bold'}}>{restaurant.item.restaurant_name}</Text>
                <Text style={{fontSize:16, fontFamily: 'OpenSans-SemiBold'}}>{restaurant.item.address.locality}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <View style={style.header}>
                <View style={style.backIconContainer}>
                    <TouchableOpacity onPress={this.backIconPress}>
                        <Icon name="ios-arrow-round-back-outline" size={45} color="#757575" />
                    </TouchableOpacity></View>
                    
                </View>
                <View>
                <Text style={style.topTen}>All Restaurants</Text>
                </View>

                {this.props.restaurantsError ? 
                (Alert.alert(
                  'Oops!',
                  'Please refresh!',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                ):
                (<View style={style.container}>
                    {
                        this.props.restaurantsLoading ? <ActivityIndicator /> :
                            (<FlatList 
                                data = {this.props.restaurants}
                                renderItem = {this.renderRestaurant}
                                keyExtractor={(item, index) => index.toString()}
                            />)
                    }
                </View>)}
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    restaurantContainer:{
        padding:'5%',

    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer:{
        marginTop: '5%',
        marginLeft: '5%',
    },
    searchbar: {
        flex:1,
        //borderColor: '#fff',
        backgroundColor:"#fff",
        //borderWidth: 1,
        alignItems: 'center',
        borderRadius:15,
        //marginTop: '5%',
        marginLeft: '5%',
        marginRight:'5%'
    },
    topTen: {
        paddingLeft: '5%',
        paddingTop: '2%',
        fontFamily: 'OpenSans-ExtraBold',
        fontSize: 22,
        color: '#212121'
    }
})

const mapStateToProps = state => {
    return {
       restaurants: state.restaurant.restaurants,
       restaurantsError: state.restaurant.restaurantsError,
       restaurantsLoading: state.restaurant.restaurantsLoading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        //recommendDishDispatch: (dish_id) => dispatch(recommendDishDispatch(dish_id)),
        fetchSelectedRestaurant: (restaurantId) => dispatch(fetchSelectedRestaurant(restaurantId))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(AllRestaurants);
