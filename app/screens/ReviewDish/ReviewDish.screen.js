import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
  NativeModules,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert, 
  ActivityIndicator
} from 'react-native';

import { connect } from 'react-redux';
import { addReview, resetReview } from '../../actions/reviews.action';
import { fetchRestaurants, fetchRestaurantsSuccess, fetchRestaurantsFailure} from '../../actions/restaurant.action';
import { fetchDishMappings } from '../../actions/dish.action';
import { Rating, AirbnbRating } from 'react-native-ratings';

var ImagePicker = NativeModules.ImageCropPicker;

class ReviewDish extends Component {

  constructor(props){
      super(props);
      this.state = {
        image: null,
        images: null,
        review: '',   
        searchedRestaurants: [],
        searchedDishes: [],     
        selectedRestaurantName: (this.props.selectedDish && this.props.selectedDish.restaurant_name) || this.props.selectedRestaurant.restaurant_name,
        selectedRestaurantId: (this.props.selectedDish && this.props.selectedDish.restaurant_id) || this.props.selectedRestaurant._id,
        selectedDishName: (this.props.selectedDish && this.props.selectedDish.dish_name),
        selectedDishId: (this.props.selectedDish && this.props.selectedDish._id),
        rating: 0,
        alreadyReviewed: this.props.alreadyReviewed || null,
      };
  }

  componentDidMount() {
    this.props.fetchRestaurants();
    this.props.resetReview();    
  }

  renderImage(image) {
    return <Image style={{width: 150, height: 150, resizeMode: 'contain'}} source={image} />
  }

  renderAsset(image) {
    return this.renderImage(image);
  }

  pickMultiple() {
    ImagePicker.openPicker({
      multiple: true,
      //includeBase64: true,
      waitAnimationEnd: false,
      includeExif: true,
    }).then(images => {
      this.setState({
        image: null,
        images: images.map(i => {
          console.log('received image', i);
          return {uri: i.path, width: i.width, height: i.height, mime: i.mime};
        })
      });
    }).catch(e => alert(e));
  }

  addReview() {
    let formData = new FormData();
    let imageDetails = this.state.images;
    var photo = [];
    var i=0;
    var item;
    if(this.state.selectedDishId === null){
      Alert.alert(
        'Oops!',
        'Please select a dish!',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }else if(this.state.selectedRestaurantId === null){
      Alert.alert(
        'Oops!',
        'Please select a restaurant!',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }else if(this.state.review==='' && this.state.rating===0){
      Alert.alert(
        'Oops!',
        'Please write rewiew or rating!',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }else{
    formData.append('mapping_id', this.state.selectedDishId);
    formData.append('restaurant_id', this.state.selectedRestaurantId);
    
    //if review is already given then send it or if not then send current review
    if(this.state.alreadyReviewed && this.state.alreadyReviewed.review)
      formData.append('review', this.state.alreadyReviewed.review);
    else
      formData.append('review', this.state.review);
   
    //if rating is already given then send it or if not then send current rating
    if(this.state.alreadyReviewed && this.state.alreadyReviewed.rating)
      formData.append('rating', this.state.alreadyReviewed.rating);
    else
      formData.append('rating', this.state.rating);
    
    if(this.state.alreadyReviewed!=null)
      formData.append('alreadyReviewedId', this.state.alreadyReviewed._id);
    else 
      formData.append('alreadyReviewedId',null);
    
    if(imageDetails !== null) {
      for (i = 0; i < imageDetails.length; i++) {
        photo[i] = {
          mime: imageDetails[i].mime,
          name: "image"+i,
          uri: imageDetails[i].uri,
        }
      }
      photo.forEach((photo) => {
        formData.append('photo', {
          uri: photo.uri,
          type: 'image/jpeg', // or photo.type
          name: photo.name
        });
      });
    }
    this.props.addReview(formData);
  }
  }

  handleReview = (text)=> {
    this.setState({ review: text })
  }

  handleRestaurant = (text) => {
    var searchedRestaurants = this.props.restaurants.filter(function(restaurant) {
        return restaurant.restaurant_name.toLowerCase().indexOf(text.toLowerCase()) > -1;
      });
    this.setState({searchedRestaurants: searchedRestaurants});
    this.setState({ selectedRestaurantName: text })
  }

  handleDish = (text) => {
    if(this.props.dishMappings){
    var searchedDishes = this.props.dishMappings.filter(function(dish) {
        if(dish.dish_name)
        return dish.dish_name.toLowerCase().indexOf(text.toLowerCase()) > -1;
      });
    this.setState({ searchedDishes: searchedDishes});
    this.setState({ selectedDishName: text })
    }
  }

  _handlePressRestaurant = (restaurant) => {
    this.setState({ selectedRestaurantName: restaurant.restaurant_name, selectedRestaurantId: restaurant._id, searchedRestaurants: []});
   // this.setState ( { selectedRestaurantId: restaurant._id});      
   // this.setState ( { searchedRestaurants: []});
    this.props.fetchDishMappings(restaurant._id);     
  }

  _handlePressFood = (dish) => {
    this.setState({ selectedDishName: dish.dish_name, selectedDishId: dish._id, searchedDishes: []});
   /*  this.setState ( { selectedDishId: dish._id })        
    this.setState ( { searchedDishes: []}); */
    //console.log("in handle press food item", this.state.itemName);       
  }

  renderRestaurant = (restaurant) => {
    return (
      <View           
      style= {styles.listItem}>
        <Text 
          style={styles.listItemText}
          onPress={() => this._handlePressRestaurant(restaurant.item)}
        >
        {restaurant.item.restaurant_name}</Text>
      </View>
    );
  };

  renderDish = (dish) => {
      return (
        <View           
        style= {styles.listItem}>
          <Text 
            style={styles.listItemText}
            onPress={() => this._handlePressFood(dish.item)}
          >
          {dish.item.dish_name}</Text>
        </View>
      );
  };

  ratingCompleted = (rating) => {
    this.setState({rating: rating});
  }

    render(){  
      let reviewFormView =null;
      let ratingView = null;
      let reviewView = null;
      if(this.state.alreadyReviewed && this.state.alreadyReviewed.review){
        reviewView = (
        <View pointerEvents="none" style={{marginLeft:10,
              marginRight:10,
              marginTop:10,
              borderColor: '#dfdfdf',
              borderWidth: 1,
              borderRadius:5}}>
              <TextInput style = {[styles.textBox]}
              multiline = {true}
              numberOfLines = {4}
              placeholder = "Review"
              underlineColorAndroid='transparent'
              value = { this.state.alreadyReviewed.review}
              autoCapitalize = "none"
              onChangeText = {this.handleReview}/></View>
        )
      }else {
        reviewView = (
          <View style={{marginLeft:10,
            marginRight:10,
            marginTop:10,
            borderColor: '#dfdfdf',
            borderWidth: 1,
            borderRadius:5}}>
            <TextInput style = {[styles.textBox]}
            multiline = {true}
            numberOfLines = {4}
            placeholder = "Review"
            underlineColorAndroid='transparent'
            value = { this.state.review}
            autoCapitalize = "none"
            onChangeText = {this.handleReview}/></View>
        )
      }

      if(this.state.alreadyReviewed && this.state.alreadyReviewed.rating){
        ratingView = (
          <View>
          <View style={{alignItems:'center', flexDirection:'row', justifyContent:'center'}}>
            <Text style={{fontFamily:'OpenSans-SemiBold', fontSize:22}}>You have already rated it </Text>
            <Text style={{fontFamily:'OpenSans-SemiBold', fontSize:22}}>{this.state.alreadyReviewed.rating}</Text>
          </View>
          <View pointerEvents="none" style={[styles.ratingContainer]}>
          <AirbnbRating 
            ratingColor={"#ffd400"} 
            count={5}
            reviews={["Bad","Okayish","Good","Very Good","Recommend"]}
            defaultRating={0}
            size={30}
            onFinishRating={this.ratingCompleted}
          /></View>
          </View>
        )
      }else {
        ratingView = (
          <View style={[styles.ratingContainer]}>
          <AirbnbRating 
            ratingColor={"#ffd400"} 
            count={5}
            reviews={["Bad","Okayish","Good","Very Good","Recommend"]}
            defaultRating={0}
            size={30}
            onFinishRating={this.ratingCompleted}
          /></View>
        )
      }
      if(this.props.newReview){
          reviewFormView = (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <Text style={{fontSize:20, fontFamily:'OpenSans-SemiBold',color:'#757575'}}>Review Added Successfully!</Text></View>         
          )}
        else {
          if(this.props.newReviewLoading){
            reviewFormView =( <ActivityIndicator/>)
          }
          else{
        reviewFormView = (
            <ScrollView>
            <View style={[styles.textBoxView]}>
            <TextInput style={[styles.textBox]}
              placeholder = "Restaurant"
              underlineColorAndroid='transparent'
              value = {this.state.selectedRestaurantName}
              onChangeText = {this.handleRestaurant}/></View>
        
              {this.props.restaurantsLoading ? <Text>Loading...</Text>
                :
              <FlatList
              data = {this.state.searchedRestaurants}
              renderItem = {this.renderRestaurant}
              keyExtractor={(item, index) => index.toString()}
              />
              }
        
              <View style={[styles.textBoxView]}>
                <TextInput style={[styles.textBox]}
                  placeholder="Dish"
                  underlineColorAndroid='transparent'
                  value={this.state.selectedDishName}
                  onChangeText={this.handleDish} />
              </View>
               
              {this.props.dishMappingsLoading ? <Text>Loading...</Text>
                   :
              <FlatList
              data={this.state.searchedDishes}
              renderItem={this.renderDish}
              keyExtractor={(item, index) => index.toString()}
              />
              }

              <View>
                {reviewView}
              </View>
             
        
              <View>
                {ratingView}
              </View>
              
        
            <View style={{marginTop: 10,flexWrap:'wrap', flexDirection: 'row'}}>
            
              {this.state.image ? this.renderAsset(this.state.image) : null}
              {this.state.images ? this.state.images.map(i => <View key={i.uri}>{this.renderAsset(i)}</View>) : null}
            </View>
            <TouchableOpacity onPress={this.pickMultiple.bind(this)} style={styles.addImagesButton}>
              <Text style={styles.text}>Add Images</Text>
            </TouchableOpacity>
            
            <TouchableOpacity style={styles.submitButton}
              onPress = {
                 () => this.addReview()
              }>
              <Text style = {styles.submitButtonText}>Submit</Text>
            </TouchableOpacity>
            
            </ScrollView>
          )}
        }
      
    return(
    <View style={{flex:1, backgroundColor:'#fff'}}>
    { reviewFormView }
    </View>
  );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addReview: (formData) => dispatch(addReview(formData)),
    fetchDishMappings: (restaurant_id) => dispatch(fetchDishMappings(restaurant_id)),
    fetchRestaurants: () => dispatch(fetchRestaurants()),
    resetReview: () => dispatch(resetReview())
  };
};

const mapStateToProps = (state) => ({
  dishMappings: state.dish.dishMappings,
  dishMappingsLoading: state.dish.dishMappingsLoading,
  dishMappingsError: state.dish.dishMappingsError,
  isLoading: state.ui.isLoading,
  restaurants: state.restaurant.restaurants,
  restaurantsLoading: state.restaurant.restaurantsLoading,
  restaurantsError: state.restaurant.restaurantsError,
  newReview: state.reviews.newReview,
  newReviewLoading: state.reviews.newReviewLoading,
  newReviewError: state.reviews.newReviewError
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addImagesButton: {
    backgroundColor: '#009e96',
    borderRadius: 16,
    margin: 18,
    height: 35,
    alignItems: 'center',
    justifyContent : 'center'
  },
  text: {
    color: '#fff',
    fontSize: 15,
    fontFamily:'OpenSans-Regular',
    textAlign: 'center'
  },
  submitButton: {
    backgroundColor: '#ffd400',
    borderRadius: 16,
    margin: 18,
    height: 50,
    alignItems: 'center',
    justifyContent : 'center'
  },
  submitButtonText: {
    color: '#fff',
    fontSize: 19,
    fontFamily:'OpenSans-Regular',
    textAlign: 'center'
  },
  textBox:{
    textAlign:'left',
    fontSize: 16,
    textAlign: 'auto',
    fontFamily: 'OpenSans-SemiBold',
    color: '#757575',
  },
  textBoxView:{
    marginTop:10,
    marginLeft:10,
    marginRight:10,
    borderBottomColor: '#dfdfdf',
    borderColor: 'transparent',
    borderWidth: 1,
  },
  ratingContainer:{
    marginLeft:10,
  },
  listItem:{
    marginLeft:10,
    padding: 8,
  },
  listItemText: {
    fontSize:18,
    fontFamily: 'OpenSans-Regular'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ReviewDish);