import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  ActivityIndicator,
  Alert
} from 'react-native';

import { connect } from 'react-redux';
import { fetchMenu } from '../../actions/menu.action';
import { recommendRestaurantDispatch} from '../../actions/restaurant.action';
import RestaurantMenu from '../../components/RestaurantMenu/RestaurantMenu';
import RestaurantCard from '../../components/RestaurantCard/RestaurantCard';

class RestaurantDetail extends Component {

  componentDidMount() {
    console.log("selected restaurant",this.props.id);
    let restaurantId = this.props.id || this.props.searchedRestaurant._id;
    this.props.fetchMenu(restaurantId);       
  }

  renderRestaurantMenuCategory = (item) => (
    <RestaurantMenu 
      title={item.item.category}
      menu={item.item.dishes}
      onDishPressed={this.dishPressedHandler}  
      >
    </RestaurantMenu >
  )

  dishPressedHandler = (item) => {
    this.props.navigator.push({
      screen: "DishDetailScreen",
      passProps: {
        selectedDish: item
      }
    })
  }

  // recommendButtonPressHandler = (restaurant) => {
  //   console.log("details--------", restaurant);
  //   this.props.recommendRestaurantDispatch(restaurant._id);
  // }

  recommendButtonPressHandler = restaurant => {
    let alreadyRecommended = false;
    //dispatch action to increase recommendation count, pass dish_restaurant_mapping id
    console.log(" user recommendations",this.props.recommendations)
    for(let i=0; i< this.props.recommendations.length; i++) {
        if(this.props.recommendations[i]._id === restaurant._id) {
            console.log("you have already recommended this restaurant");
            alreadyRecommended = true;
            Alert.alert(
                '',
                'you have already recommended this restaurant',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
                { cancelable: false }
            )
            break;
        } 
    }
    if(alreadyRecommended === false) {
      this.props.recommendRestaurantDispatch(restaurant._id);
    }
}
  // reviewButtonPressHandler = (restaurant) => {
  //   this.props.navigator.push({
  //     screen: "ReviewDishScreen",
  //     title: 'Add Review',
  //     passProps: {
  //       selectedRestaurant: restaurant
  //     }
  //   });
  // }

  reviewButtonPressHandler = restaurant => {
    console.log("Dish--------", restaurant);
    let alreadyReviewed = false;
    //dispatch action to increase recommendation count, pass dish_restaurant_mapping id
    console.log(" user reviews", this.props.reviews)
    for (let i = 0; i < this.props.reviews.length; i++) {
      if(this.props.reviews[i].restaurant_detail){
        if (this.props.reviews[i].restaurant_detail._id === restaurant._id) {
            alreadyReviewed=true
            if(this.props.reviews[i].review.review === "") {
                console.log("you have already rated this restaurant");
                Alert.alert(
                    '',
                    'you have already rated this restaurant. Do you want to write a Review?',
                    [
                        { text: 'OK', onPress: () => this.moveToReviewPage(restaurant, this.props.reviews[i].review)},
                        { text: 'Cancel'}
                    ],
                   
                )
                break;
            } else if(this.props.reviews[i].review.rating === 0 ){
                console.log("you have already reviewed this restaurant. Do you want to rate it?");
                Alert.alert(
                    '',
                    'you have already reviewed this restaurant. Do you want to rate it?',
                    [
                        { text: 'OK', onPress: () => this.moveToReviewPage(restaurant, this.props.reviews[i].review)},
                        { text: 'Cancel'}
                    ],
                   
                )
                break;
            }
            
            else {
                Alert.alert(
                    '',
                    'you have already reviewed it. Please check your profile for more detail.',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                        { text: 'Cancel', }
                    ],
                    
                )
                break;
            }
        }
    }
  }
    if(alreadyReviewed===false)
    this.moveToReviewPage(restaurant, null);
}

moveToReviewPage = (restaurant, alreadyReviewed) => {
    console.log("moving to review page.", restaurant);
    console.log("already review----------", alreadyReviewed);
    this.props.navigator.push({
        screen: "ReviewDishScreen",
        title: 'Add Review',
        passProps: {
            selectedRestaurant: restaurant,
            alreadyReviewed: alreadyReviewed
        }
    });
}
  render() {
    console.log("props---------", this.props)
    let menuList = <FlatList
      data={this.props.menu}
      keyExtractor={(item, index) => index.toString()}
      renderItem={this.renderRestaurantMenuCategory} />
    if(this.props.isLoading) (
      menuList = <ActivityIndicator/>
    )
    let restaurantDetail = this.props.searchedRestaurant || this.props.selectedRestaurant
    return(
      <View>
        <ScrollView>
        {this.props.selectedRestaurantError ? 
                (Alert.alert(
                  'Oops!',
                  'Please refresh!',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                ):
          (<View>
            {
              (this.props.selectedRestaurantLoading) ? <ActivityIndicator/> : (
                <RestaurantCard
                  restaurantName={restaurantDetail.restaurant_name}
                  restaurantCuisines={restaurantDetail.cuisines}
                  restaurantBuilding={restaurantDetail.address.building}
                  restaurantStreet={restaurantDetail.address.street}
                  restaurantLocality={restaurantDetail.address.locality}
                  restaurantFamousDishes={restaurantDetail.famous_dishes}
                  restaurantOpenTime={restaurantDetail.open_time}
                  restaurantCloseTime={restaurantDetail.close_time}
                  restaurantAverageCost={restaurantDetail.average_cost_for_two}
                  restaurantCategory={restaurantDetail.category}
                  restaurantImage={restaurantDetail.images}
                  restaurantRating={restaurantDetail.average_rating}
                  restaurantContact={restaurantDetail.phone_number}
                  onRecommendButtonPressed={() => this.recommendButtonPressHandler(restaurantDetail)}
                  onReviewButtonPressed={() => this.reviewButtonPressHandler(restaurantDetail)}
                />
              )
            }
          
          </View>)}
          {this.props.menuError ? 
                (Alert.alert(
                  'Oops!',
                  'Please refresh!',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                ):
          (<View elevation={5} style={styles.menuContainer}>
            {menuList}
          </View>)}
        </ScrollView>
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  
  menuContainer: {
    margin: 11,
    borderRadius: 10,
    backgroundColor: '#ffffff',
  },
  
})

const mapStateToProps = (state) => ({
    reviews: state.reviews.reviews,
    reviewsError: state.reviews.reviewsError,
    reviewsLoading: state.reviews.reviewsLoading,
    recommendations: state.reviews.recommendations,
    recommendationsError: state.reviews.recommendationsError,
    recommendationsLoading: state.reviews.recommendationsLoading,
    menu: state.menu.menu,
    menuLoading: state.menu.menuLoading,
    menuError: state.menu.menuError,
    isLoading: state.ui.isLoading,
    selectedRestaurant: state.restaurant.selectedRestaurant,
    selectedRestaurantLoading: state.restaurant.selectedRestaurantLoading,
    selectedRestaurantError: state.restaurant.selectedRestaurantError
});

const mapDispatchToProps = dispatch => {
  return {
    recommendRestaurantDispatch : (restaurant_id) => dispatch(recommendRestaurantDispatch(restaurant_id)),
    fetchMenu: (restaurantId) => dispatch(fetchMenu(restaurantId))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantDetail) ;