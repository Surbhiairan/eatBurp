import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ActivityIndicator,
    Alert
} from 'react-native';

import DishList from '../../components/DishList/DishList';
import SearchBar from '../../components/SearchBar/SearchBar';
import dishes from '../../data/data';
import { connect } from 'react-redux';
import { recommendDishDispatch, fetchTopDishes } from '../../actions/dish.action';
import { fetchSelectedRestaurant } from '../../actions/restaurant.action';
import Icon from 'react-native-vector-icons/Ionicons';

class TopTenDish extends Component {

    static navigatorStyle = {
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    componentDidMount(){
        console.log("in top ten cdm");
        this.props.fetchTopDishes();
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress") {
            if (event.id === "sideDrawerToggle") {
                this.props.navigator.toggleDrawer({
                    side: "left"
                });
            }
        }
    };

    state = {
        dishes: dishes
    }
    
    recommendButtonPressHandler = dish => {
        let alreadyRecommended = false;
        //dispatch action to increase recommendation count, pass dish_restaurant_mapping id
        console.log(" user recommendations",this.props.recommendations)
        for(let i=0; i< this.props.recommendations.length; i++) {
            if(this.props.recommendations[i]._id === dish._id) {
                console.log("you have already recommended this dish");
                alreadyRecommended = true;
                Alert.alert(
                    '',
                    'you have already recommended this dish',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                    { cancelable: false }
                )
                break;
            } 
        }
        if(alreadyRecommended === false) {
            this.props.recommendDishDispatch(dish._id);
        }
    }

    reviewButtonPressHandler = dish => {
        console.log("Dish--------", dish);
        let alreadyReviewed = false;
        //dispatch action to increase recommendation count, pass dish_restaurant_mapping id
        console.log(" user reviews", this.props.reviews)
        for (let i = 0; i < this.props.reviews.length; i++) {
            if(this.props.reviews[i].dish_detail){                
            if (this.props.reviews[i].dish_detail._id === dish._id) {
                alreadyReviewed=true
                if(this.props.reviews[i].review.review === "") {
                    console.log("you have already rated this dish");
                    Alert.alert(
                        '',
                        'you have already rated this dish. Do you want to write a Review?',
                        [
                            { text: 'OK', onPress: () => this.moveToReviewPage(dish, this.props.reviews[i].review)},
                            { text: 'Cancel'}
                        ],
                       
                    )
                    break;
                } else if(this.props.reviews[i].review.rating === 0 ){
                    console.log("you have already reviewed this dish. Do you want to rate it?");
                    Alert.alert(
                        '',
                        'you have already reviewed this dish. Do you want to rate it?',
                        [
                            { text: 'OK', onPress: () => this.moveToReviewPage(dish, this.props.reviews[i].review)},
                            { text: 'Cancel'}
                        ],
                       
                    )
                    break;
                }
                
                else {
                    Alert.alert(
                        '',
                        'you have already reviewed it. Please check your profile for more detail.',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                            { text: 'Cancel', }
                        ],
                        
                    )
                    break;
                }
            }
        }
    }
        if(alreadyReviewed===false)
        this.moveToReviewPage(dish, null);
    }

    moveToReviewPage = (dish, alreadyReviewed) => {
        console.log("moving to review page.", dish);
        console.log("already review----------", alreadyReviewed);
        this.props.navigator.push({
            screen: "ReviewDishScreen",
            title: 'Add Review',
            passProps: {
                selectedDish: dish,
                alreadyReviewed: alreadyReviewed
            }
        });
    }

    searchBarPressHandler = () => {
        this.props.navigator.push({
            screen: "SearchSuggestionScreen",
        });
    }

    backIconPress = () => {
        this.props.navigator.pop();
    }

    restaurantPressedHandler = (dish) => {
        console.log("here------------", dish)
        this.props.fetchSelectedRestaurant(dish.restaurant_id);
        this.props.navigator.push({
            screen: "RestaurantDetailScreen",
            title: dish.restaurant_name,
            passProps: {
                id: dish.restaurant_id
            }
        });
    }

    render() {
        console.log("inside render-----------", this.props);
        let dishList = <DishList
            dishes={this.props.topDishes}
            onRecommendButtonPressed={this.recommendButtonPressHandler}
            onReviewButtonPressed={this.reviewButtonPressHandler}
            onRestaurantPressed={this.restaurantPressedHandler}
        />
        if (this.props.isLoading) (
            dishList = <ActivityIndicator />
        )
        return (
            <View style={{ backgroundColor: '#fff', flex: 1 }}>
                <View style={style.header}>
                <View style={style.backIconContainer}>
                    <TouchableOpacity onPress={this.backIconPress}>
                        <Icon name="ios-arrow-round-back-outline" size={45} color="#fff" />
                    </TouchableOpacity></View>
                    <View style={style.searchbar}>
                        <SearchBar
                            onSearchBarPressed={this.searchBarPressHandler}
                        />
                    </View >
                </View>
                <View>
                <Text style={style.topTen}>Top Ten Dishes</Text>
                </View>

                {this.props.topDishesError ? 
                (Alert.alert(
                  'Oops!',
                  'Please refresh!',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                ):
                (<View style={style.container}>
                   {dishList}
                </View>)}
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        backgroundColor:'#009e96',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop:'2%',
        paddingBottom:'1%'
    },
    backIconContainer:{
        //marginTop: '5%',
        marginLeft: '5%',
    },
    searchbar: {
        flex:1,
        //borderColor: '#fff',
        backgroundColor:"#fff",
        //borderWidth: 1,
        alignItems: 'center',
        borderRadius:15,
        //marginTop: '5%',
        marginLeft: '5%',
        marginRight:'5%'
    },
    topTen: {
        paddingLeft: '5%',
        paddingTop: '1%',
        paddingBottom: '1%',        
        fontFamily: 'OpenSans-SemiBold',
        fontSize: 22,
        color: '#212121'
    }
})

const mapStateToProps = state => {
    console.log("state-------", state)
    return {
        isLoading: state.ui.isLoading,
        topDishes: state.dish.topDishes,
        topDishesLoading: state.dish.topDishesLoading,
        topDishesError: state.dish.topDishesError,
        reviews: state.reviews.reviews,
        reviewsError: state.reviews.reviewsError,
        reviewsLoading: state.reviews.reviewsLoading,
        recommendations: state.reviews.recommendations,
        recommendationsError: state.reviews.recommendationsError,
        recommendationsLoading: state.reviews.recommendationsLoading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchTopDishes: () => dispatch(fetchTopDishes()),        
        recommendDishDispatch: (dish_id) => dispatch(recommendDishDispatch(dish_id)),
        fetchSelectedRestaurant: (restaurantId) => dispatch(fetchSelectedRestaurant(restaurantId))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(TopTenDish);
