import React, { Component } from 'react';
import { View, 
        Text, 
        Button, 
        StyleSheet, 
        KeyboardAvoidingView, 
        Keyboard,
        TouchableWithoutFeedback,
        ActivityIndicator,
        Image
    } from 'react-native';
import {connect} from 'react-redux';

import { tryAuth, authAutoSignIn } from '../../actions/auth.action';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import HeadingText from '../../components/UI/HeadingText/HeadingText';
import MainText from '../../components/UI/MainText/MainText';
import ButtonWithBackground from '../../components/UI/ButtonWithBackground/ButtonWithBackground';
import validate from '../../utility/validation';

const DEFAULT_IMAGE = require('../../data/images/logo.png');
const eatburp = require('../../data/images/eatburp.png');

class AuthScreen extends Component {

    static navigatorStyle = {
        navBarHidden: true
    };

    state = {
        authMode: "login",
        controls: {
            firstName: {
                value: "",
                valid: false,
                validationRules: {
                    minLength: 1
                },
                touched: false
            },
            lastName :{
                value: "",
                valid: false,
                validationRules: {
                    minLength: 1
                },
                touched: false
            },
            email: {
                value: "",
                valid: false,
                validationRules: {
                    isEmail: true
                },
                touched: false
            },
            password: {
                value: "",
                valid: false,
                validationRules: {
                    minLength: 6
                },
                touched: false
            },
            confirmPassword: {
                value: "",
                valid: false,
                validationRules: {
                    equalTo: "password"
                },
                touched: false
            }
        }
    };

    componentDidMount() {
        this.props.onAutoSignIn();
    }
    authHandler = () => {
        const authData = {
            firstName: this.state.controls.firstName.value,
            lastName: this.state.controls.lastName.value,
            email: this.state.controls.email.value,
            password: this.state.controls.password.value
        };
        this.props.onTryAuth(authData, this.state.authMode);
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {
                authMode: prevState.authMode === 'login' ? 'signup' : 'login'
            }
        })
    }

    updateInputState = (key, value) => {
        let connectedValue = {};
        if (this.state.controls[key].validationRules.equalTo) {
            const equalControl = this.state.controls[key].validationRules.equalTo;
            const equalValue = this.state.controls[equalControl].value;
            connectedValue = {
                ...connectedValue,
                equalTo: equalValue
            };
        }
        if (key === "password") {
            connectedValue = {
                ...connectedValue,
                equalTo: value
            };
        }
        this.setState(prevState => {
            return {
                controls: {
                    ...prevState.controls,
                    confirmPassword: {
                        ...prevState.controls.confirmPassword,
                        valid:
                            key === "password"
                                ? validate(
                                    prevState.controls.confirmPassword.value,
                                    prevState.controls.confirmPassword.validationRules,
                                    connectedValue
                                )
                                : prevState.controls.confirmPassword.valid
                    },
                    [key]: {
                        ...prevState.controls[key],
                        value: value,
                        valid: validate(
                            value,
                            prevState.controls[key].validationRules,
                            connectedValue
                        ),
                        touched: true
                    }
                }
            };
        });
    };

    render() {
        let confirmPasswordControl = null;
        let firstNameControl = null;
        let lastNameControl = null;
        let submitButton = (
            <ButtonWithBackground
                color="#ffd400"
                onPress={this.authHandler}
                disabled={
                    !this.state.controls.confirmPassword.valid && this.state.authMode === 'signup' ||
                    !this.state.controls.email.valid ||
                    !this.state.controls.password.valid
                }
            >
                Submit
            </ButtonWithBackground>
        )
        if(this.state.authMode === 'signup') {
            confirmPasswordControl = (
                <DefaultInput
                    placeholder="Confirm Password"
                    style={styles.input}
                    value={this.state.controls.confirmPassword.value}
                    onChangeText={(val) => this.updateInputState('confirmPassword', val)}
                    valid={this.state.controls.confirmPassword.valid}
                    touched={this.state.controls.confirmPassword.touched}
                    secureTextEntry
                />
            );
            firstNameControl = (
                <DefaultInput
                    placeholder="First Name"
                    style={styles.input}
                    value={this.state.controls.firstName.value}
                    onChangeText={(val) => this.updateInputState('firstName', val)}
                    valid={this.state.controls.firstName.valid}
                    touched={this.state.controls.firstName.touched}
                    autoCapitalize = "none"
                    autoCorrect = {false}
                />
            );
            lastNameControl = (
                <DefaultInput
                    placeholder="Last Name"
                    style={styles.input}
                    value={this.state.controls.lastName.value}
                    onChangeText={(val) => this.updateInputState('lastName', val)}
                    valid={this.state.controls.lastName.valid}
                    touched={this.state.controls.lastName.touched}
                    autoCapitalize = "none"
                    autoCorrect = {false}
                />
            )
        }

        if(this.props.isLoading) {
            submitButton = <ActivityIndicator />;
        }
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding">
            <View style={{height:'20%'}}>
            <Image
            resizeMode="contain"
            source={eatburp}
            style={{flex:1, height: 100, width: 200}}
          />               
            </View>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.inputContainer}>
                    {firstNameControl}
                    {lastNameControl}
                    
                        <DefaultInput
                            placeholder="Your E-Mail Address"
                            style={styles.input}
                            value = {this.state.controls.email.value}
                            onChangeText = {(val) => this.updateInputState('email', val)}
                            valid = {this.state.controls.email.valid}
                            touched = {this.state.controls.email.touched}
                            autoCapitalize = "none"
                            autoCorrect = {false}
                            keyboardType = "email-address"
                        />
                        <DefaultInput 
                            placeholder="Password" 
                            style={styles.input} 
                            value={this.state.controls.password.value}
                            onChangeText={(val) => this.updateInputState('password', val)}
                            valid={this.state.controls.password.valid}
                            touched={this.state.controls.password.touched}
                            secureTextEntry
                        />
                        {confirmPasswordControl}
                    </View>
                </TouchableWithoutFeedback>
                {submitButton}
                <ButtonWithBackground 
                    color="#009e96" 
                    onPress={this.switchAuthModeHandler}>
                    Switch to {this.state.authMode === 'login'? 'Sign up' : 'Login'}
                </ButtonWithBackground>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        //backgroundColor: "#ffd400"
    },
    heading: {
        fontFamily:'CarterOne', 
        fontSize: 40, 
        color:'#363636', 
        fontWeight: 'bold'
    },
    backgroundImage: {
        width: "100%",
        flex: 1
    },
    inputContainer: {
        width: "80%"
    },
    input: {
        borderBottomColor: '#b6b6b6',
        borderBottomWidth: 1,
        // backgroundColor: "#eee",
        borderColor: "#bbb"
    },
    imageStyle:{
        height:200,
        width:200
    }
});

const mapsStateToProps = state => {
    return {
        isLoading: state.ui.isLoading
    }
}

const mapsDispatchToProps = dispatch => {
    return {
        onTryAuth: (authData, authMode) => dispatch(tryAuth(authData, authMode)),
        onAutoSignIn: () => dispatch(authAutoSignIn())
    };
};

export default connect(mapsStateToProps, mapsDispatchToProps)(AuthScreen);