import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Dimensions,
  Image
} from 'react-native';

import LikeIcon from '../../components/SvgIcons/like.icon';
import PenIcon from '../../components/SvgIcons/pen.icon';

const DEFAULT_IMAGE = require('../../data/images/cs3.jpg');
const win = Dimensions.get('window');

const CustomImage = (props) => {
  console.log(props, "propsssssssssssssssss");
  if (props.dishImages.length > 0) {
    return (
      <View >
        <ImageBackground
          //resizeMode="contain"
          imageStyle={{ borderRadius: 10 }}
          style={{ width: win.width*.9, height: win.height * .6, }}
          source={{ uri: props.dishImages[0] }} >
          <Rating average_rating={props.average_rating} />
        </ImageBackground>
      </View>
    )
  }
  else
    return (
      <View>
        <ImageBackground
          //resizeMode="contain"
          imageStyle={{ borderRadius: 10 }}
          source={DEFAULT_IMAGE}
          style={{  alignSelf: 'center',width: win.width*.9 , height: win.height * .6, }} >
          <Rating average_rating = {props.average_rating} />
        </ImageBackground>
      </View>
    )
}

const Recommendations = (props) => {
  if(props.recommended > 0) {
    return (
      <Text style={{ color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 16, marginLeft: 16 }}>
        {props.recommended} Recommendations
    </Text>
    )
  }
   else {return null}
}

const Rating = (props) => {
  if (props.average_rating > 0) {
    return (
      <View style={styles.textOnImageContainer}>
        <Text style={styles.textOnImage}>{props.average_rating}</Text>
      </View>
    )
  }
  else { return (
    <View style={styles.textOnImageContainer}>
      <Text style={{
        fontSize: 18,
        color: '#fff',
        fontFamily: 'OpenSans-Regular'}}>New</Text>
    </View>
  ) }
}

const Reviews = (props) => {
  if(props.reviews.length > 0) {
    return (
      <View>
        <Text style={{ color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 18, marginLeft: 10, marginTop: 5, marginBottom: 2 }}>
        Reviews
      </Text>
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={props.reviews}
        renderItem={(item) => {
          return (
            <View elevation={1} style={{ paddingBottom:4,marginTop:4,marginBottom:4,marginLeft: 8, marginRight: 8, borderRadius: 4 }}>
              <View style={{ flexDirection: 'row', margin: 4 }}>
                <View style={{flex:6, flexDirection:'row'}}>
                <Text style={{ color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 15, paddingLeft: 4 }}>{item.item.user.first_name}</Text>
                <Text style={{ color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 15, paddingLeft: 4 }}>{item.item.user.last_name}</Text>
                </View>
                <View style={{flex:1, alignItems:'center', backgroundColor:'#009e96', borderRadius:8}}>
                <Text style={{ color: '#fff', fontFamily: 'OpenSans-Bold', fontSize: 14, }}>{item.item.rating}</Text>
                </View>
              </View>
              <Text style={{ color: '#212121', fontFamily: 'OpenSans-Regular', fontSize: 14, paddingLeft: 4, marginLeft: 4 }}>{item.item.review}</Text>
              <View style={{flex:1,alignItems:'center'}}>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                numColumns={3}
                data={item.item.images}
                renderItem={({ item }) =>                       
                  <Image
                    source={{ uri: item }}
                    style={{ margin:5,width: 100, height: 100 }}
                  />
                }
              /></View>
            </View>
          )
        }}
      />
      </View>
    )
  } else {
    return (
      <Text style={{ color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 14, marginLeft: 10, marginTop: 5, marginBottom: 2 }}>
        No Reviews
      </Text>
    )
  }
}

const dishCard = (props) => {
  roundedRating = props.average_rating.toFixed(1)
  return (  
    <View elevation={5} style={styles.dishCard} >
      
        <CustomImage
          average_rating={this.roundedRating}
          dishImages={props.dish_images} />
      <View style={{ flexDirection: 'row', marginLeft: '3%'}}>
        <View 
        style={{
           height:50, 
           width:50, 
           borderRadius:25,  
           backgroundColor:'#ffd400', 
           alignItems:'center', 
           justifyContent:'center',
           margin:'1%'}}>
        <TouchableOpacity onPress={props.onRecommendButtonPressed} style={{ padding: 4 }}>
          <LikeIcon fill={'#000'} height={26} width={26} />
        </TouchableOpacity></View>

        <View 
        style={{
          height:50, 
          width:50, 
          borderRadius:25,  
          backgroundColor:'#ffd400', 
          alignItems:'center', 
          justifyContent:'center',
          margin:'1%'}}>
          <TouchableOpacity onPress={props.onReviewButtonPressed} style={{  }}>
          <PenIcon fill={'#000'} height={24} width={24} />
        </TouchableOpacity></View>

        <View style={{flex:5,margin:'1%', alignItems:'flex-end'}}>
          <Recommendations recommended = {props.recommended} /></View>
      </View>
      <View style={{flexDirection: 'row',marginLeft:'3%', marginRight:'3%'}}>
      <View style={{flex:3}}>
        <Text style={{color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 18}}>
          {props.dish_name}
        </Text></View>
        <View style={{flex:1,alignItems:'flex-end'}}>
        <Text style={{color: '#212121', fontFamily: 'OpenSans-SemiBold', fontSize: 18}}>
          {props.price}/-
        </Text></View>
      </View>
      <TouchableOpacity onPress={props.onRestaurantPressed}>
      <Text style={{ color: '#646464', fontFamily: 'OpenSans-SemiBold', fontSize: 18, marginLeft: 10, paddingTop: 2, paddingBottom: 2,}}>
        {props.restaurant_name}, {props.locality}
      </Text>
      </TouchableOpacity>
      <Reviews reviews = {props.reviews}/>
    </View>

  )
}

const styles = StyleSheet.create({
  dishCard: {
    flex: 1,
    backgroundColor: '#fff',
    borderRadius: 10,
    // paddingLeft: 5,
    // paddingRight: 5,
    paddingBottom: 5,
    paddingTop: 5,
    marginLeft:'3%',
    marginRight:'3%',
    marginTop:'1%',
    marginBottom:'1%'
  },
  textOnImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    borderRadius: 10,
    backgroundColor: '#ffd400',
    padding: 2,
    margin: 8
  },
  textOnImage: {
    fontSize: 24,
    color: '#fff',
    fontFamily: 'OpenSans-Bold'
  },
  imageStyle: {
    flexWrap: 'wrap',
  },
  
});

export default dishCard;