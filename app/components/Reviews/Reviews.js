import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Image,
  } from 'react-native';
    
const reviews = (props) => {
  console.log("props on review page--------", props);
  if(props.review.dish_detail){
    return(
    <View elevation={1} style ={{padding:10,borderRadius:5, margin: 3,backgroundColor : "#fff"}}>      
      <View><Text style={{fontFamily:'OpenSans-Semibold', fontSize:15,color:'#009e96'}}>DISH</Text></View>
      <View style={{flexDirection:'row'}}>
      <View style={{flex:6, flexDirection:'row'}}>
        <Text style={{color:"#212121", fontFamily: "OpenSans-SemiBold", fontSize: 15, }}>
          {props.review.dish_detail.dish_name}
        </Text>
      </View>
      <View style={{flex:1, alignItems:'center', backgroundColor:'#009e96', borderRadius:8}}>
      <Text style={{color: '#fff', fontFamily: 'OpenSans-Bold', fontSize: 14,}}>
        {props.review.review.rating}
      </Text></View>
      </View>

      <View style={{flexDirection:'row'}}>
      <Text style={{color:"#757575", fontFamily: "OpenSans-Bold", fontSize: 13}}>{props.review.dish_detail.restaurant_name},</Text>
      <Text style={{paddingLeft: 4, color:"#757575", fontFamily: "OpenSans-Bold", fontSize: 13}}>{props.review.dish_detail.locality}</Text>
      </View>
      
      <Text style={{color:"#212121", fontFamily: "OpenSans-Regular", fontSize: 14}}>{props.review.review.review}</Text>
      <View style={{flex:1,alignItems:'center'}}>
      <FlatList 
        keyExtractor={(item, index) => index.toString()}
        numColumns = {3}
        data={props.review.review.images}
        renderItem = {({item})=>
         <Image 
          source={{uri: item}}
          style={{margin:5,width: 100, height: 100}}
          />
        }
      /></View>
    </View>
    );
  }else{
    return(
      <View elevation={1} style ={{padding:10,borderRadius:5, margin: 3,backgroundColor : "#fff"}}>
      <View><Text style={{fontFamily:'OpenSans-Semibold', fontSize:15,color:'#009e96'}}>RESTAURANT</Text></View>
      <View style={{flexDirection:'row'}}>
      <View style={{flex:6, flexDirection:'row'}}>
        <Text style={{color:"#212121", fontFamily: "OpenSans-SemiBold", fontSize: 15,}}>
          {props.review.restaurant_detail.restaurant_name},
        </Text>
        <Text style={{paddingLeft: 4, color:"#212121", fontFamily: "OpenSans-SemiBold", fontSize: 15, paddingLeft: 4 }}>
          {props.review.restaurant_detail.address.locality}
        </Text>
      </View>
      <View style={{flex:1, alignItems:'center', backgroundColor:'#009e96', borderRadius:8}}>
      <Text style={{color: '#fff', fontFamily: 'OpenSans-Bold', fontSize: 14,}}>
        {props.review.review.rating}
      </Text></View>
      </View>

      <Text style={{color:"#212121", fontFamily: "OpenSans-Regular", fontSize: 14}}>
        {props.review.review.review}
      </Text>

      <View style={{flex:1,alignItems:'center'}}>
      <FlatList 
        keyExtractor={(item, index) => index.toString()}
        numColumns = {3}
        data={props.review.review.images}
        renderItem = {({item})=>
         <Image 
          source={{uri: item}}
          style={{margin:5,width: 100, height: 100}}
          />
        }
      /></View>
    </View>
    )
  }
};

var styles = StyleSheet.create({
  imageStyle: {
    width: '100%', 
    height: 200
  },
})
export default reviews;